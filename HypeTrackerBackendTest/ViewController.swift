//
//  ViewController.swift
//  HypeTrackerBackendTest
//
//  Created by Luke Mann on 3/1/17.
//  Copyright © 2017 Luke Mann. All rights reserved.
//

import UIKit
import SwiftyJSON
import ScrollableGraphView

class ViewController: UIViewController {

    let graphView = ScrollableGraphView(frame: CGRect(x: 0, y: 0, width: 650, height: 400))
    
    var count:[Double]=[]
    //TODO: Make graph from counts in this array
    var datesCount:[Double]=[]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpJSON()

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func setUpJSON() {
        let url = URL(string: "https://access.alchemyapi.com/calls/data/GetNews?apikey= ca3a7106b52aafc125610650469f362f98297fcf&start=1487721600&end=1489014000&q.enriched.url.text=adidas%20yeezy&q.enriched.url.taxonomy.taxonomy_.label=style%20and%20fashion&count=25&outputMode=json&timeSlice=1d")
        var jsonString: String?
        do {
            jsonString=try String(contentsOf: url!)
        } catch let error {
            print(":(")
        }
        if let dataFromString = jsonString?.data(using: .utf8, allowLossyConversion: false) {
            let json = JSON(data: dataFromString)
            jsonStuff(json: json)
        }
    }
    
    func jsonStuff(json:JSON){
        let entries=json["result"]["slices"]
        var i = 0.0
        for (key,subJson):(String,JSON) in entries {
            i+=1.0
        }
        let total=i+1
        for (key,subJson):(String,JSON) in entries {
            let date=Double((subJson.stringValue as? String)!)
            datesCount.append(date!)
            count.append(date!*(total-i))
            i-=1.0
        }
        
        operateOnDates()
    }
    
    func operateOnDates() {
        let datesSum = count.reduce(0, +)
        print("\(datesSum)%")
    datesCount.append(90.0)
        
        let labels:[String]=["One", "Two"]
        
        graphView.set(data: datesCount, withLabels: labels)
        
graphView.shouldFill=true
        graphView.fillType = .gradient
        graphView.fillGradientStartColor = .white
        graphView.fillGradientEndColor = UIColor(red: 230/255, green: 153/255, blue: 175/255, alpha: 1.0)
        graphView.shouldAnimateOnStartup = true
        graphView.shouldFill = true
        graphView.shouldAnimateOnAdapt = true
        self.view.addSubview(graphView)
    }

}

